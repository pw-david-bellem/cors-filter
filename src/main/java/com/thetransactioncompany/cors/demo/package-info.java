/**
 * Demo servlet for testing the CORS filter.
 *
 * @author Vladimir Dzhuvinov
 */
package com.thetransactioncompany.cors.demo;